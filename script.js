define(['jquery'], function ($) {
	var CustomWidget = function () {
		var self = this;
		var subdomain = AMOCRM.constant('account').subdomain;
		this.dateToTimestamp = function (date) {
			var dateParts = date.split('.');
			return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]).getTime() / 1000;
		};
		this.getCurrentDate = function () {
			var date = new Date();
			var year = date.getFullYear();

			var month = (1 + date.getMonth()).toString();
			month = month.length > 1 ? month : '0' + month;

			var day = date.getDate().toString();
			day = day.length > 1 ? day : '0' + day;
			return day + '.' + month + '.' + year;
		};
		this.getNotes = function (ids, offset = 0) {
			var result;
			$.ajax({
				url: 'https://' + subdomain + '.amocrm.ru/api/v2/notes',
				async: false,
				type: 'GET',
				data: {
					type: 'lead',
					limit_rows: 500,
					limit_offset: offset,
					element_id: ids,
					note_type: [1, 3]
				}
			})
			.done(function (data) {
				if (data) {
					result = data._embedded.items;
				}
			});
			return result;
		};
		this.getAjaxNote = function (leadsId) {
			var notes = [];
			leadsId.forEach(function (ids, i) {
				var part = self.getNotesLeads(ids);
				notes = notes.concat(part);
			});
			return notes;
		};
		this.getNotesLeads = function (ids) {
			var notes = [];
			function getNotes (ids, offset = 0) {
				var result;
				$.ajax({
					url: 'https://' + subdomain + '.amocrm.ru/api/v2/notes',
					async: false,
					type: 'GET',
					data: {
						type: 'lead',
						limit_rows: 500,
						limit_offset: offset,
						element_id: ids,
						note_type: [1, 3]
					}
				})
				.done(function (data) {
					if (data) {
						result = data._embedded.items;
					}
				});
				return result;
			}

			function checkLength (ids, offset = 0) {
				var part = getNotes(ids, offset);
				notes = notes.concat(part);
				if (part.length >= 500) {
					offset += 500;
					checkLength(ids, offset);
				}
			}
			checkLength(ids);
			return notes;
		};
		this.getAjaxList = function (page = 1, leadsId = []) {
			var ids = self.ajaxList(page);
			leadsId.push(ids);
			if (ids.length >= 500) {
				page++;
				self.getAjaxList(page, leadsId);
			}
			return leadsId;
		};
		this.ajaxList = function (page = 1) {
			var result = [];
			var strGET = window.location.search.replace('?', '');
			$.ajax({
				url: 'https://' + subdomain + '.amocrm.ru/ajax/leads/list/' + '?' + strGET,
				async: false,
				type: 'POST',
				dataType: 'json',
				data: {
					json: true,
					PAGEN_1: page,
					ELEMENT_COUNT: 500
				}
			})
			.done(function (data) {
				data.response.items.forEach(function (item, i) {
					result.push(item.id);
				});
			});
			return result;
		};
		this.ajaxLeads = function (createds) {
			var leads;
			$.ajax({
				url: 'https://' + subdomain + '.amocrm.ru/api/v2/leads',
				async: false,
				type: 'GET',
				dataType: 'json',
				data: {id: createds}
			})
			.done(function (data) {
				leads = data._embedded.items;
			});
			return leads;
		};
		this.callbacks = {
			render: function () {
				$(document).ready(function () {
					var currentEntity = AMOCRM.data.current_entity;
					if ((currentEntity === 'leads') || (currentEntity === 'leads-pipeline')) {
						var elem = $('.list__top__actions');
						if ($(elem).find('#leads_status_report').length < 1) {
							$(elem).append(
								'<div id="leads_status_report" class="button-input button-input_blue">' +
									'<span class="button-input-inner__text">Отчёт по статусам</span>' +
								'</div>'
							);
						}
					}
				});
				return true;
			},
			init: function () {
				return true;
			},
			bind_actions: function () {
				function showPreloaderNova () {
					$('body').append('<div class="nova_preloader" style="position: absolute;' +
						'top: 0;' +
						'left: 0;' +
						'width: 100%;' +
						'height: 100%;' +
						'background: rgba(255,255,255,.5);' +
						'z-index: 9999999999999;' +
					'"><div class="text" style="' +
						'font-size: 40px;' +
						'color: #333;' +
						'text-align: center;' +
						'position: absolute;' +
						'width: 100%;' +
						'left: 0;' +
						'top: 50%;' +
					'">Загрузка...</div></div>');
					$('.nova_preloader .text').css({
						'font-size': '40px',
						'color': '#333',
						'text-align': 'center',
						'position': 'absolute',
						'width': '100%',
						'left': '0',
						'top': '50%'
					});
				}

				function hidePreloaderNova () {
					$('.nova_preloader').hide();
				}
				$('#leads_status_report').click(function () {
					var fromDate = prompt('Начало периода', self.getCurrentDate());
					var toDate = prompt('Конец периода', self.getCurrentDate());
					if (fromDate && toDate) {
						showPreloaderNova();
						var leadsId = self.getAjaxList(1);
						var notes = self.getAjaxNote(leadsId);
						var result = {};
						fromDate = self.dateToTimestamp(fromDate);
						toDate = self.dateToTimestamp(toDate) + 86400;
						var createds = [];
						notes.forEach(function (note, i) {
							if ((note.created_at >= fromDate) && (note.created_at <= toDate)) {
								// Считаем новые статусы
								if (note.note_type === 3) {
									result[note.params.STATUS_NEW] = result[note.params.STATUS_NEW] ? parseInt(result[note.params.STATUS_NEW]) + 1 : 1;
								// Иначе записываем id  в массив только что созданных
								} else {
									createds.push(note.element_id);
								}
							}
						});
						if (createds.length > 0) {
							var leads = self.ajaxLeads(createds);
							top:
							for (var l = 0; l < leads.length; l++) {
								for (var n = 0; n < notes.length; n++) {
									if ((leads[l]['id'] == notes[n]['element_id']) && (notes[n]['note_type'] == 3)) {
										if ((notes[n]['created_at'] >= fromDate) && (notes[n]['created_at'] <= toDate)) {
											var key = notes[n].params.STATUS_OLD;
											if (result[key]) {
												result[key] = parseInt(result[key]) + 1;
											} else {
												result[key] = 1;
											}
										}
										leads.splice(l, 1);
										l--;
										continue top;
									}
								}
							}
							leads.forEach(function (elem, i) {
								if (result[elem.status_id]) {
									result[elem.status_id] = parseInt(result[elem.status_id]) + 1;
								} else {
									result[elem.status_id] = 1;
								}
							});
							$.get('https://' + subdomain + '.amocrm.ru/ajax/v1/pipelines/list', {}, function (data) {
								var statusesArray = [];
								var pipelines = data.response.pipelines;
								top:
								for (var key in result) {
									for (var p in pipelines) {
										for (var s in pipelines[p].statuses) {
											if (key == pipelines[p].statuses[s].id) {
												statusesArray.push([
													pipelines[p].statuses[s].name,
													result[key]
												]);
												continue top;
											}
										}
									}
								}
								$.post('https://script.google.com/macros/s/AKfycbzV7Lsteesr3pXk5Oig7UdakbH1_RpfmasFHxzs1WpdXI2DyU75/exec', {
									doc: '1BUANMAPm1wFn3hueqnIzp1Xnd8JokyO6hdWPQfQXd7o',
									sheet: 0,
									data: JSON.stringify(statusesArray)
								}, function (data) {
									hidePreloaderNova();
									prompt('Ссылка на документ', data);
								});
							});
						}
					}
				});
				return true;
			},
			settings: function () {
				return true;
			},
			onSave: function () {
				return true;
			},
			destroy: function () {
			},
			contacts: {
				// select contacts in list and clicked on widget name
				selected: function () {
				}
			},
			leads: {
				// select leads in list and clicked on widget name
				selected: function () {
				}
			},
			tasks: {
				// select taks in list and clicked on widget name
				selected: function () {
				}
			}
		};
		return this;
	};

	return CustomWidget;
});
